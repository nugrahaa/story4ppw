from django.contrib import admin
from django.urls import path, include

from . import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.index),
    path('about/', views.about),
    path('skills/', views.skills),
    path('educations/', views.educations),
    path('organizations/', views.organizations),
    path('contacts/', views.contacts),
    path('kegiatanku/', include('kegiatanku.urls'))
]
