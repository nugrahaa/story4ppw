from django.shortcuts import render

def index(request):
    context = {
        'judul' : 'Homepage'
    }
    return render(request, 'index.html', context)

def about(request):
    context = {
        'judul' : 'About Page'
    }
    return render(request, 'about.html', context)

def skills(request):
    context = {
        'judul' : 'My Skill'
    }
    return render(request, 'skills.html', context)

def educations(request):
    context = {
        'judul' : 'Educations'
    }

    return render(request, 'educations.html', context)

def organizations(request):
    context = {
        'judul' : 'Organizations'
    }

    return render(request, 'organizations.html', context)

def contacts(request):
    context = {
        'judul' : 'Contacts'
    }

    return render(request, 'contacts.html', context)