from django.shortcuts import render, redirect

from .models import Kegiatan
from .forms import KegiatanForm

def listKegiatan(request):
    kegiatans = Kegiatan.objects.all()

    context = {
        'judul' : 'List Kegiatan',
        'kegiatans' : kegiatans,
    }

    return render(request, 'kegiatanku/list.html', context)


def createKegiatan(request):
    kegiatan_form = KegiatanForm(request.POST or None)

    if request.method == "POST":
        if kegiatan_form.is_valid():
            kegiatan_form.save()

            return redirect('list')

    context = {
        'judul' : 'Buat Kegiatan',
        'kegiatan_form' : kegiatan_form,
    }

    return render(request, 'kegiatanku/create.html', context)


def delete(request, delete_id):
    Kegiatan.objects.filter(id=delete_id).delete()
    return redirect('list')
