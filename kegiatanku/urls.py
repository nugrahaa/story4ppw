from django.urls import path, re_path

from . import views

urlpatterns = [
    path('', views.listKegiatan, name='list'),
    path('create/', views.createKegiatan, name='create'),
    re_path('delete/(?P<delete_id>.*)/', views.delete, name='delete'),
]