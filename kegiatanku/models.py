from django.db import models

class Kegiatan(models.Model):
    nama_kegiatan   = models.CharField(max_length=255)

    LIST_HARI = (
        ('Minggu', 'Minggu'),
        ('Senin', 'Senin'),
        ('Selasa', 'Selasa'),
        ('Rabu', 'Rabu'),
        ('Kamis', 'Kamis'),
        ('Jumat', 'Jumat'),
        ('Sabtu', 'Sabtu'),
    )

    hari            = models.CharField(
        max_length = 100,
        choices = LIST_HARI,
        default = 'Senin',
    )
    
    tanggal         = models.DateField(auto_now=False, auto_now_add=False)
    jam             = models.TimeField(auto_now=False, auto_now_add=False)
    tempat          = models.CharField(max_length=255)
    kategori        = models.CharField(max_length=255)


    def __str__(self):
        return "{}.{}".format(self.id, self.nama_kegiatan)
    