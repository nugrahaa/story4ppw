from django import forms

from .models import Kegiatan

class KegiatanForm(forms.ModelForm):
    class Meta:
        model = Kegiatan

        fields = [
            'nama_kegiatan',
            'hari',
            'tanggal',
            'jam',
            'tempat',
            'kategori',
        ]

        widgets = {
            'nama_kegiatan' : forms.TextInput (
                attrs = {
                    'class' : 'form-control',
                    'placeholder' : 'isi dengan judul kegiatan'
                }
            ),

            'hari' : forms.Select (
                attrs = {
                    'class' : 'form-control',
                    
                }
            ),

            'tanggal' : forms.TextInput (
                attrs = {
                    'class' : 'form-control form-column',
                    'type' : 'date',
                }
                
            ),

            'jam' : forms.TextInput (
                attrs = {
                    'class' : 'form-control',
                    'type'  : 'time',
                }
            ),

            'tempat' : forms.TextInput (
                attrs = {
                    'class' : 'form-control',
                    'placeholder' : 'ex : Belyos'
                }
            ),

            'kategori' : forms.TextInput (
                attrs = {
                    'class' : 'form-control',
                    'placeholder' : 'ex : Belajar'
                }
            ),
        } 
